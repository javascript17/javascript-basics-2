let isOfficer = true;
let age = 12;
let isGeneral = true;
let isKing = true;

if (isOfficer && age >= 18) {
    alert("tYou are allowed to enter.");
}

console.log("This is after the if statement");

if (isOfficer && isKing) {
    console.log("You are allowed to enter.");
}

console.log("This is after the if statement");

let number = 21;

if (number % 2 == 0) {
    console.log("The number is even.")
} else {
    console.log("The number is odd")
}

let pet = "snail";

if (pet == "dog") {
    console.log("Puppy!");
} else if (pet == "cat") {
    console.log("Meow.")
} else if (pet == "snail") {
    console.log("...")
} else {
    console.log("Try again.")
}

// male or female?

let gender = "female";

let output = (gender == "male") ? "You are a male" : "You are a female";
console.log(output);

let answer = prompt("How are you?");
console.log(answer);

// Create a function that will greet the user upon loading the page
// Lalabas Mr. or Mrs based sa gender

function welcome(gender, name) {
    name = prompt("What is your name?");
    gender = prompt("What is your gender?");
    if (gender.toLowerCase() == "male") {
        alert(`Welcome, Mr. ${name}!`)
    } else if (gender.toLowerCase() == "female") {
        alert(`Welcome, Ms. ${name}!`)
    } else {
        alert(`Welcome, ${name}!`)
    }
}

welcome();