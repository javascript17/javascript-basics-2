// This function will ask an input from the user and will determine if the said input is a number or not.

function numbercheck() {
    num = prompt("Please input a number.")
    if(isNaN(num) || num == "" || num == " ") {
        console.log(`${num} is not a number. I told you to put a number!`)
    } else {
        console.log(`${num} is a number.`)
    }
}


// This function will determine if the year given by the user is a leap year or not. The said function will notify the user if the input is a valid year.

function leapyear() {
    year = prompt("Please input a year.")
    if(isNaN(year) || year == "" || year == " ") {
        console.log(`${year} is not a year.`)
    } else {
        if (year % 4 != 0) {
            console.log(`Year ${year} is not a leap year.`)
        } else if (year % 100 != 0) {
            console.log(`Year ${year} is a leap year.`)
        } else if (year % 400 != 0) {
            console.log(`Year ${year} is not a leap year.`)
        } else {
            console.log(`Year ${year} is a leap year.`)
        }
    }
}


// This function will tell the user if the input grade is passing or not.

function gradecheck() {
    grade = prompt("This is simple grade check. Please input your grade.")
    if (grade >= 62 && grade <= 99) {
        if (grade >= 75 && grade <=99) {
            console.log(`Your grade ${grade} is a passing grade. Congratulations!`)
        } else {
            console.log(`Your grade ${grade} is a failing grade. It's okay, try next time.`)
        }
    } else {
        console.log(`${grade} is not a valid grade.`)
    }
}

function gradecheck2() {
    g1 = prompt("We will calculate the average of your 5 exams. Please input grade 1.")
    g2 = prompt("We will calculate the average of your 5 exams. Please input grade 2.")
    g3 = prompt("We will calculate the average of your 5 exams. Please input grade 3.")
    g4 = prompt("We will calculate the average of your 5 exams. Please input grade 4.")
    g5 = prompt("We will calculate the average of your 5 exams. Please input grade 5.")

    if (g1 >= 62 && g2 >= 62 && g3 >= 62 && g4 >= 62 && g5 >= 62 && g1 <= 99 && g2 <= 99 && g3 <= 99 && g4 <= 99 && g5 <= 99) {
       average = ((Number(g1) + Number(g2) + Number(g3) + Number(g4) + Number(g5))/5)
       console.log(average);
       if (average >= 75 && average <=99) {
        console.log(`Your average is ${average}. Congratulations, you passed!`)
       } else {
        console.log(`Your average is ${average}. You failed.`)
       }
    } else {
        console.log(`Invalid input from user.`)
    }
}

numbercheck();
leapyear();
gradecheck();
gradecheck2();